$(function(){

	clickGaleria();
	initGaleria();

	function initGaleria(){
		var img = $('.mini-foto-wraper').eq(0).css('background-image');
		$('.foto-destaque').css('background-image',img);
	}

	var elOffTop = $('.foto-destaque').offset().top;

	function clickGaleria(){

		$('.mini-foto').click(function(){

			$('.mini-foto').css('background-color','transparent');
			$(this).css('background-color','rgb(220,220,220)');

			var img = $(this).children().css('background-image');

			$('.foto-destaque').css('background-image',img);
			$('html,body').animate({scrollTop:elOffTop});
		});
	}
});	
