$(function(){

	//click button contato

	var directory = '/Projetos/Projeto_06/';

	$('[goto=contato]').click(function(){
		location.href=directory+'?contato';
		return false;
	});

	checkUrl();

	function checkUrl(){

		var url = location.href.split('/');
		var curPage = url[url.length-1].split('?');

		if (curPage[1] != undefined && curPage[1] == 'contato') {

			var windowResponsive = $(window).width();

			if (windowResponsive >= 1500) {

				var elOff = $('#contato').offset().top;
				$('[goto=contato]').css('color','black');
				$('html,body').animate({scrollTop:elOff - 100},600);
				return false;

			} else if (windowResponsive >= 768 && windowResponsive < 1500 ) {

				var elOff = $('#contato').offset().top;
				$('[goto=contato]').css('color','black');
				$('html,body').animate({scrollTop:elOff - 300},600);
				return false;

			} else if (windowResponsive > 580 && windowResponsive <= 768) {

				var half1 = $('.servicos-depoimentos .half1').innerHeight();	
				var elOff = $('#contato').offset().top;
				$('[goto=contato]').css('color','black');
				$('html,body').animate({scrollTop:elOff - half1 + 100},600);
				return false;

			} else if(windowResponsive > 380 && windowResponsive <= 580){

				var half1 = $('.servicos-depoimentos .half1').innerHeight();	
				var elOff = $('#contato').offset().top;
				$('[goto=contato]').css('color','black');
				$('html,body').animate({scrollTop:(elOff - half1) + 200},600);
				return false;

			} else if(windowResponsive <= 380){

				var half1 = $('.servicos-depoimentos .half2').innerHeight();	
				var elOff = $('#contato').offset().top;
				$('[goto=contato]').css('color','black');
				$('html,body').animate({scrollTop:(elOff - half1 + 395)},600);
				return false;
				
			}
		} else{
			$('a[href='+curPage[0]+']').css('color','black');
		}
	}		
});